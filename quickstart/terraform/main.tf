terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "2.76.0"
    }

  }
}

provider "azurerm" {
  features {} 
}

resource "azurerm_resource_group" "kibana-monitoring-quickstart-rg" {
  name     = "kibana-monitoring-quickstart-rg"
  location = "UK South"
}

resource "azurerm_virtual_network" "kibana-monitoring-quickstart-vnet" {
  name                = "kibana-monitoring-quickstart-vnet"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.kibana-monitoring-quickstart-rg.location
  resource_group_name = azurerm_resource_group.kibana-monitoring-quickstart-rg.name
}

resource "azurerm_subnet" "subnet-01" {
  name                 = "subnet-01"
  resource_group_name  = azurerm_resource_group.kibana-monitoring-quickstart-rg.name
  virtual_network_name = azurerm_virtual_network.kibana-monitoring-quickstart-vnet.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_public_ip" "kibana-monitoring-quickstart-vm-public-ip" {
  name = "kibana-monitoring-quickstart-vm-public-ip"
  location = "UK South"
  resource_group_name = azurerm_resource_group.kibana-monitoring-quickstart-rg.name
  allocation_method = "Dynamic"
  sku = "Basic"
}


resource "azurerm_network_interface" "kibana-monitoring-quickstart-vm-nic" {
  name                = "kibana-monitoring-quickstart-vm-nic"
  location            = azurerm_resource_group.kibana-monitoring-quickstart-rg.location
  resource_group_name = azurerm_resource_group.kibana-monitoring-quickstart-rg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.subnet-01.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.kibana-monitoring-quickstart-vm-public-ip.id
  }
}

resource "azurerm_linux_virtual_machine" "elastic-single-node-cluster-vm" {
  name                = "elastic-single-node-cluster-vm"
  resource_group_name = azurerm_resource_group.kibana-monitoring-quickstart-rg.name
  location            = azurerm_resource_group.kibana-monitoring-quickstart-rg.location
  size                = "Standard_F2"
  admin_username      = "elastic"
  network_interface_ids = [
    azurerm_network_interface.kibana-monitoring-quickstart-vm-nic.id,
  ]

  admin_ssh_key {
    username   = "elastic"
    public_key =  file(".ssh/single-node-elastic-cluster.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.5"
    version   = "latest"
  }

# Install and run Elasticsearch and Kibana
 connection {
    host = "${azurerm_linux_virtual_machine.elastic-single-node-cluster-vm.public_ip_address}"
    user = "elastic"
    type = "ssh" 
    private_key = file(".ssh/single-node-elastic-cluster")
    timeout = "2m"
  }

  provisioner "remote-exec" {
     script = "install-elasticsearch-and-kibana.sh"
  }

}