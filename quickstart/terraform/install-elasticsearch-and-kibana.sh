#!/usr/bin/env bash

IP_ADDRESS=$(hostname -i | cut -d' ' -f1)
# (the 'cut' command is needed as 'hostname' adds a trailing space)

# set the open file handler limit
echo "elastic - nofile 65536" | sudo tee -a /etc/security/limits.conff

# set memory map limits
echo "vm.max_map_count=262144" | sudo tee -a /etc/sysctl.conf

# set the heap
echo "-Xms2g" | sudo tee -a /home/elastic/elasticsearch/config/jvm.options
echo "-Xmx2g" | sudo tee -a /home/elastic/elasticsearch/config/jvm.options

# download, configure, and deploy Elasticsearch
curl -O https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.14.1-linux-x86_64.tar.gz
tar -xvzf *.gz
rm *.gz
mv elasticsearch* elasticsearch

cd elasticsearch
sed -i -e 's/#cluster.name:.*/cluster.name: kibana_auditing/g' config/elasticsearch.yml
sed -i -e "s/#node.name:.*/node.name: master-1/g" config/elasticsearch.yml
sed -i -e 's/#network.host:.*/network.host: [_local_, _site_]/g' config/elasticsearch.yml
sed -i -e "s/#discovery.seed_hosts:.*/discovery.seed_hosts: [\"$IP_ADDRESS\"]/g" config/elasticsearch.yml
sed -i -e "s/#cluster.initial_master_nodes: .*/cluster.initial_master_nodes: [\"master-1\"]/g" config/elasticsearch.yml
sed -i -e "s/#node.roles:.*/node.roles: [master]/g" config/elasticsearch.yml

# start Elasticsearch
./bin/elasticsearch -d -p pid



